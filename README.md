# Mästarspelet

A game of skill for the Commodore Amiga, played with a tennis ball, a
golf club and some christmas decoration.

It is an ugly quick hack made hours before the new year's party 2023.

The main flow of the game is:

 0. The program is started on an Amiga.
 1. All players read the rules on the intro screen, and go through the
    registering of players.
 2. Following the instructions on the screen, players try to hit
    targets in the room with a golf club and a tennis ball. Results
    are entered into the program.
 3. When a winning condition is met, the program declares the winner.


Picture of the party game kit:

![](assets/kit.jpg){width=60%}
![](assets/scrshot0.png){width=60%}
![](assets/scrshot1.png){width=60%}


## Install build dependencies

First install the build kit. Translate to your OS's packaging system.
```
apt-get update
apt-get install lhasa gcc build-essential curl bc rsync
```

Then create a file `settings.mk` where you defined where the
[cross compiler](http://www.compilers.de/vbcc.html) will be installed,
for example:
```
export VBCC := /tmp/vb
```

Then build the cross compiler:
```
make -f Makefile.install_vb
```

## Build Amiga binaries

This will build [src/mastarspelet.c](src/mastarspelet.c) into `target/mastarspelet.kick13` and `target/mastarspelet.aos68k` to be used in Amiga OS 1.3 and 3 respectively.
```
make
```

For reference, the whole build process can be studied and executed in [Docker or Podman](docker/Dockerfile), for example by running `docker build -f docker/Dockerfile -t mastarspelet .`.

## Run in emulator

Install the emulator:
```
apt-get install fs-uae
```

Add a couple of more lines to `settings.mk`, a pair of kickstart and workbench files for either or both 1.3 and 3. Please note that it can be a hassle to find pairs of kickstart and workbench files that work together:
```
export rom_kick13 = <path to kickstart 1.3 rom file>
export rom_aos68k = <path to kickstart 3 rom file>

export wb_kick13 = <path to Workbench 1.3 adf file>
export wb_aos68k = <path to Workbench 3 install adf file>
```

Then boot the emulator, selecting config using the OS variable:
```
make -f Makefile.emu OS=aos68k
make -f Makefile.emu OS=kick13
```

To run Mästarspelet from Workbench, start Amiga Shell, make it full screen. The `target` folder where
the built binaries end up is mounted as a volume called `target:` in the OS:
```
cd target:
mastarspelet.kick13
mastarspelet.aos68k
```
Obviously select the version of the game matching your OS version.


## Transfer the executable program to diskette

There are a couple of ways to transfer files from your modern
workstation to an Amiga diskette nowadays. Here is the way I did it:

 - Using the excellent [amitools](https://github.com/cnvogelg/amitools), I copied the binary into the C-folder of the Workbench diskette image:
```
xdftool wb1.3.adf write target/mastarspelet.kick13 c/mastarspelet
```
 - The modified diskette image was copied to a CF-card (formatted with the Amiga Fast File System):
```
mkdir mountpoint
sudo mount -t affs /dev/sdb1 mountpoint
cp wb1.3.adf mountpoint/
sudo umount mountpoint
```
 - I inserted the CF-card into the AUX port of the
   [aca500plus](http://wiki.icomp.de/wiki/ACA500plus) expansion card
   and booted the Amiga from its boot card (with Amiga OS 3).
 - I installed [tsgui](https://aminet.net/package/disk/misc/tsgui) in Amiga OS, which allows you to write ADF files to floppy disk.
 - Using tsgui, I wrote `wb1.3.adf` from the CF-card to an empty diskette in the disk drive.


## License

GPL v3

## Fun facts

Since the program only uses ANSI terminal codes for text formatting and standard C features, you can run
it in your GNU/Linux terminal too (even though the encoding of the
Swedish characters is wrong):
```
gcc -o mastarspelet src/mastarspelet.c
./mastarspelet
```

## Known bugs

There are probably a lot of bugs considering that the code was hacked
together in very short time and has not been tested thoroughly.

## TODO

The encoding of Swedish umlaut characters is annoying, as it looks unreadable in the
source code and is hard to manage, so a better handling of that would be nice.

Then there are a lot of game related stuff to improve:
 - The balancing of chance parameters such as FUMMELRISK should be
   improved, so it works better together with the randomised base
   skills of the players. Right now the fumble hits seems to happen
   very seldom.
 - The creature selection of the players (orch, demon etc) should be
   exploited in the calculation of scores.
 - More interesting concepts from DnD should be incorporated

## Copyright

Per Weijnitz
