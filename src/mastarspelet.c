#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <time.h>

#define MAX_LEN 255             // Maximum length of strings
#define NR_ROUNDS 3             // Number of rounds in the game (excluding sudden death)
#define MAX_SKILL_LEVEL 18      // Maximum skill level for players
#define FUMMELRISK 10           // Risk of fumble
#define MAX_PLAYERS 100         // Maximum number of players

// Here are the umlaut chars which look right in Amiga Shell, for reference:
// Å \305
// Ä \304
// Ö \326
// å \345
// ä \344
// ö \366


/* TYPE DEFS */

static const char *player_classes[] = {
  "anka",
  "dv\344rg",
  "demon",
  "enh\366rning",
  "grottroll",
  "grip",
  "h\344st",
  "lik\344tare",
  "orch"
};

#define nr_player_classes 9


typedef enum {
  intro_screen,
  enter_players_menu,
  game,
  sudden_death,
  game_over
} StateType;


typedef struct _state {
  StateType state;
  int round;
  int sudden_death_score;
  int winner;
} State;


typedef struct _player {
  char name[MAX_LEN];
  int _class;
  int score;

  int master_mode;

  int styrka;
  int fysik;
  int storlek;
  int intelligens;
  int psykisk_kraft;
  int smidighet;
  int karisma;
} Player;

/* GLOBAL VARS */
static State state;
static Player players[MAX_PLAYERS];
static int num_players = 0;


/* FUNCTION DECLARATIONS */

/* "parse" the character as a ONE DIGIT POSITIVE integer, return -1 if not a digit */
static int
parse_char_as_int (char c) {
  if (!isdigit (c)) {
    printf ("Error: '%c' is not a digit\n", c);
    return -1;
  }

  return (int) (c - '0');
}

static char
get_chr (void) {
  int c;
  int first;

  first = getchar ();

  if (first == '\n' || first == EOF) {
    return (char) first;
  }

  // discard the rest of the line
  while ((c = getchar ()) != '\n' && c != EOF) {
    continue;
  }

  return (char) first;
}

static int
get_int (int from, int to) {
  char c;
  int i;

  do {
    c = get_chr ();
    i = parse_char_as_int (c);
    if (i < from || i > to) {
      printf ("Felaktigt val. F\366rs\366k igen: ");
    }
  }
  while (i < from || i > to);

  return i;
}

static void
clear_screen () {
  printf ("\033[1;1H\033[2J");
}

static void
fg_white () {
  printf ("\033[%dm", 31);
}

static void
fg_black () {
  printf ("\033[%dm", 32);
}

//static void fg_orange() {  printf("\033[%dm", 33); }
//static void bg_white() {  printf("\033[%dm", 41); }
static void
bg_black () {
  printf ("\033[%dm", 42);
}

static void
bg_orange () {
  printf ("\033[%dm", 43);
}

static void
ansi_reset () {
  printf ("\033[m");
}

static void
print_divider (int top) {
  int x;

  if (top == 0) {
    bg_black ();
    fg_white ();
    printf ("# %-72s #", " ");
    ansi_reset ();
    printf ("\n");
  }

  bg_black ();
  fg_white ();
  for (x = 0; x < 76; x++) {
    printf ("#");
  }
  ansi_reset ();

  printf ("\n");

  if (top != 0) {
    bg_black ();
    fg_white ();
    printf ("# %-72s #\n", " ");
    ansi_reset ();
  }
}

static void
print_screen (char *lines[]) {
  int lno;

  print_divider (1);
  lno = 0;
  while (lines[lno] != NULL) {
    bg_black ();
    fg_white ();
    printf ("# ");
    ansi_reset ();

    bg_orange ();
    fg_black ();
    printf ("%-72s", lines[lno]);
    ansi_reset ();

    bg_black ();
    fg_white ();
    printf (" #\n");
    ansi_reset ();

    lno++;
  }
  print_divider (0);
}

static void
print_header (const char *format, ...) {
  va_list args;
  va_start (args, format);

  bg_black ();
  fg_white ();
  (void) vprintf (format, args);
  ansi_reset ();

  va_end (args);
}

static void
init () {
  srand ((unsigned int) time (NULL));
  state = (State) {
  .state = intro_screen,.round = 0};
}

static void
print_intro_screen (void) {
  char *title[] = {
    " ",
    " ",
    "  __  __  o o      _                            _      _    ",
    " |  \\/  | __ _ ___| |_ __ _ _ __ ___ _ __   ___| | ___| |_  ",
    " | |\\/| |/ _` / __| __/ _` | '__/ __| '_ \\ / _ \\ |/ _ \\ __| ",
    " | |  | | (_| \\__ \\ || (_| | |  \\__ \\ |_) |  __/ |  __/ |_  ",
    " |_|  |_|\\__,_|___/\\__\\__,_|_|  |___/ .__/ \\___|_|\\___|\\__| ",
    "                                    |_|                    ",
    "                                                   v0.1",
    "     - MAKTKAMPEN I STUGAN",
    " ",
    " ",
    "                     Viktiga regler:                               ",
    "                     - St\345 bakom linjen och sl\345 bollen mot m\345len,  ",
    "                     - M\345lens ben\344mning \344r 1 till 3 (1 n\344rmast),   ",
    "                     - Spelare med h\366gst po\344ng efter tre rundor vinner, ",
    "                     - Sudden death vid delad f\366rstaplats, ",
    "                     - Endast en vinnare, ",
    "                     - Spelm\344staren har sista ordet, ",
    "                     - Inga \"v\344letablerade regler\" fr\345n G\344vle ",
    "                       g\344ller (som i fuskstugans kortspel Mas) ",
    " ",
    " ",
    " [TRYCK ENTER F\326R ATT FORTS\304TTA]",
    NULL
  };
  print_screen (title);
}

static void
print_sudden_death_screen (void) {
  char *scr[] = {
    " ",
    "  ____            _     _              ____             _   _     _ ",
    " / ___| _   _  __| | __| | ___ _ __   |  _ \\  ___  __ _| |_| |__ | |",
    " \\___ \\| | | |/ _` |/ _` |/ _ \\ '_ \\  | | | |/ _ \\/ _` | __| '_ \\| |",
    "  ___) | |_| | (_| | (_| |  __/ | | | | |_| |  __/ (_| | |_| | | |_|",
    " |____/ \\__,_|\\__,_|\\__,_|\\___|_| |_| |____/ \\___|\\__,_|\\__|_| |_(_)",
    " ",
    " ",
    " [TRYCK ENTER F\326R ATT FORTS\304TTA]",
    NULL
  };
  print_screen (scr);
}

static void
print_game_over_screen (void) {
  int i, j;
  char *scr[] = {
    " ",
    "   ____                                            _ ",
    "  / ___| __ _ _ __ ___   ___    _____   _____ _ __| |",
    " | |  _ / _` | '_ ` _ \\ / _ \\  / _ \\ \\ / / _ \\ '__| |",
    " | |_| | (_| | | | | | |  __/ | (_) \\ V /  __/ |  |_|",
    "  \\____|\\__,_|_| |_| |_|\\___|  \\___/ \\_/ \\___|_|  (_)",
    "                                                     ",
    NULL
  };
  print_screen (scr);

  printf ("\n\n");

  print_header ("\n\n*** Vinnare: %s ***\n\n", players[state.winner].name);

  for (i = 0; i < num_players - 1; i++) {
    for (j = 0; j < num_players - i - 1; j++) {
      if (players[j].score < players[j + 1].score) {
        Player temp = players[j];
        players[j] = players[j + 1];
        players[j + 1] = temp;
      }
    }
  }

  printf ("\n  Po\344ng\t Namn\n");

  for (i = 0; i < num_players; i++) {
    printf ("  %d\t %s\n", players[i].score, players[i].name);
  }

  printf ("\n\n[TRYCK ENTER F\326R ATT FORTS\304TTA]");
  (void) get_chr ();
}

static void
read_string (char *str) {
  if (fgets (str, MAX_LEN, stdin) == NULL) {
    // Handle error or end-of-file here, for example by setting the first character of str to '\0'
    str[0] = '\0';
    return;
  }
  // Replace the newline character with a null character
  char *newline = strchr (str, '\n');
  if (newline) {
    *newline = '\0';
  }
}

static void
enter_players (void) {
  char c;
  int i;
  size_t len;

  clear_screen ();

  do {
    printf ("\n");
    bg_black ();
    fg_white ();
    printf ("#### Registrera spelare %d:", num_players);
    ansi_reset ();
    printf ("\n\n");

    bg_black ();
    fg_white ();
    printf ("# Namn: ");
    ansi_reset ();
    read_string (players[num_players].name);
    printf ("\n");

    players[num_players].master_mode = 0;

    len = strlen (players[num_players].name);
    while (len > 0 && players[num_players].name[len - 1] == ' ') {
      players[num_players].name[len - 1] = '\0';
      len--;
      players[num_players].master_mode = 1;
    }

    for (i = 0; i < nr_player_classes; i++) {
      printf ("    %d. %s\n", i, player_classes[i]);
    }
    printf ("\n");
    bg_black ();
    fg_white ();
    printf ("# V\344lj varelse (ange siffra): ");
    ansi_reset ();
    i = get_int (0, nr_player_classes - 1);
    players[num_players]._class = i;

    players[num_players].score = 0;

    // randomise skill levels into ranges 3..18 inclusive
    players[num_players].styrka = rand () % (MAX_SKILL_LEVEL - 2) + 3;
    players[num_players].fysik = rand () % (MAX_SKILL_LEVEL - 2) + 3;
    players[num_players].storlek = rand () % (MAX_SKILL_LEVEL - 2) + 3;
    players[num_players].intelligens = rand () % (MAX_SKILL_LEVEL - 2) + 3;
    players[num_players].psykisk_kraft = rand () % (MAX_SKILL_LEVEL - 2) + 3;
    players[num_players].smidighet = rand () % (MAX_SKILL_LEVEL - 2) + 3;
    players[num_players].karisma = rand () % (MAX_SKILL_LEVEL - 2) + 3;

    printf ("\n");
    bg_orange ();
    fg_black ();
    printf ("# Sammanfattning:");
    ansi_reset ();
    printf ("\n\n");
    printf ("  Namn:%s\n  Varelse: %s\n\n", players[num_players].name, player_classes[players[num_players]._class]);
    printf ("  Styrka: %d\n", players[num_players].styrka);
    printf ("  Fysik: %d\n", players[num_players].fysik);
    printf ("  Storlek: %d\n", players[num_players].storlek);
    printf ("  Intelligens: %d\n", players[num_players].intelligens);
    printf ("  Psykisk kraft: %d\n", players[num_players].psykisk_kraft);
    printf ("  Smidighet: %d\n", players[num_players].smidighet);
    printf ("  Karisma: %d\n", players[num_players].karisma);
    printf ("\n\n");

    do {
      bg_black ();
      fg_white ();
      printf ("# L\344gg till ytterligare spelare (j/n), eller korrigera (k) senaste: ");
      ansi_reset ();
      c = get_chr ();
    }
    while (c != 'j' && c != 'n' && c != 'k');

    if (c != 'k') {
      num_players++;
    }
  }
  while (c != 'n' && num_players < MAX_PLAYERS);
  printf ("\n");

  clear_screen ();
  bg_orange ();
  fg_black ();
  printf ("# Spelare:");
  ansi_reset ();
  printf ("\n\n");
  for (i = 0; i < num_players; i++) {
    printf ("  %d. %s, %s\n", i, players[i].name, player_classes[players[i]._class]);
  }
  printf ("\n\n\n[TRYCK ENTER F\326R ATT FORTS\304TTA]\n");
  (void) get_chr ();
}

static int
calculate_threshold (Player player, int risk) {
  int current_skill_level = player.fysik + player.smidighet + player.intelligens;
  int max_skill_level = MAX_SKILL_LEVEL * 3;    // 3 skills

  // Linear interpolation
  int threshold = risk - (current_skill_level * risk / max_skill_level);

  return threshold;
}

static void
player_round (int player) {
  int hit;
  int d20;
  int thres;

  printf ("\n");
  bg_black ();
  fg_white ();
  printf ("## Spelare %d: %s", player, players[player].name);
  ansi_reset ();
  printf ("\n");
  printf ("   Po\344ng: %d\n\n", players[player].score);

  printf ("\n");
  print_header ("Sl\345 bollen mot m\345len och ange resultat (0 = miss, 1-3 = tr\344ff p\345 m\345l): ");
  hit = get_int (0, 3);

  if (hit == 0) {
    printf ("\n");
    print_header ("Spelm\344starens kommentar: inte bra, inte bra alls - miss!");
    printf ("\n");
  }
  if (hit == 3) {
    printf ("\n");
    print_header ("Spelm\344starens kommentar: oerh\366rt bra, oerh\366rt bra");
    printf ("\n");
  }

  printf ("\n");
  print_header ("Amiga 500 ber\344knar slutligt slagresultat...");
  printf ("\n");
  d20 = rand () % 20 + 1;

  thres = calculate_threshold (players[player], FUMMELRISK);
  if (hit > 0 && d20 <= thres && players[player].master_mode == 0) {
    print_header (" - Fummelslag! Po\344ngavdrag. (T20 %d <= tr\366skelv\344rde %d)", d20, thres);
    printf ("\n");
    print_header
      (" - %d = risk %d - (FYS %d + SMI %d + INT %d) * risk %d / (max %d * antal 3)",
       thres, FUMMELRISK, players[player].fysik, players[player].smidighet, players[player].intelligens, FUMMELRISK, MAX_SKILL_LEVEL);
    printf ("\n");
    hit--;
  }
  else if (d20 == 20) {
    print_header (" - Perfekt slag! Bonuspo\344ng. (T20 = 20)");
    printf ("\n");
    hit++;
  }

  print_header (" - Slagresultat: %d", hit);
  printf ("\n");

  players[player].score += hit;
  if (hit != 0) {
    printf ("\n");
    print_header ("Po\344ng registrerad: %d", hit);
    printf ("\n\n");
  }
  printf ("\n[TRYCK ENTER F\326R ATT FORTS\304TTA]\n");
  (void) get_chr ();
}

static void
play (void) {
  int player;

  clear_screen ();
  printf ("\n");
  print_header ("#### Spelrunda %d", state.round);
  printf ("\n\n");

  for (player = 0; player < num_players; player++) {
    player_round (player);
  }
}

static void
play_sudden_death (void) {
  int player;
  clear_screen ();
  printf ("\n");
  print_header ("###### Sudden Death!");
  printf ("\n\n");
  print_header ("#### Spelrunda %d", state.round);
  printf ("\n\n");

  for (player = 0; player < num_players; player++) {
    if (players[player].score < state.sudden_death_score) {
      continue;
    }
    player_round (player);
  }
}

static StateType
check_game_over (void) {
  int i;
  StateType new_game = game;

  new_game = state.state;

  if (state.state == game && state.round >= NR_ROUNDS) {
    int max_score = 0;
    int max_score_player = 0;
    int max_score_players = 0;
    for (i = 0; i < num_players; i++) {
      if (players[i].score > max_score) {
        max_score = players[i].score;
        max_score_players = 1;
        max_score_player = i;
      }
      else if (players[i].score == max_score) {
        max_score_players++;
      }
    }

    if (max_score_players == 1) {
      state.winner = max_score_player;
      new_game = game_over;
    }
    else {
      new_game = sudden_death;
      state.sudden_death_score = max_score;
      clear_screen ();
      print_sudden_death_screen ();
      (void) get_chr ();
    }
  }
  else if (state.state == sudden_death) {
    int max_score = 0;
    int max_score_players = 0;
    int max_score_player = 0;
    for (i = 0; i < num_players; i++) {
      if (players[i].score > max_score) {
        max_score = players[i].score;
        max_score_players = 1;
        max_score_player = i;
      }
      else if (players[i].score == max_score) {
        max_score_players++;
      }
    }

    if (max_score_players == 1) {
      new_game = game_over;
      state.winner = max_score_player;
    }
    else {
      state.sudden_death_score = max_score;
    }
  }

  return new_game;
}

static void
main_loop () {
  int stop = 0;
  while (stop == 0) {
    switch (state.state) {
    case intro_screen:
      print_intro_screen ();
      (void) get_chr ();
      state.state = enter_players_menu;
      break;
    case enter_players_menu:
      enter_players ();
      state.state = game;
      break;
    case game:
      play ();
      state.round++;
      state.state = check_game_over ();
      break;
    case sudden_death:
      play_sudden_death ();
      state.round++;
      state.state = check_game_over ();
      break;
    case game_over:
      clear_screen ();
      print_game_over_screen ();
      stop = 1;
      break;
    }
  }
}

int
main () {
  clear_screen ();
  init ();
  main_loop ();

  return (0);
}
