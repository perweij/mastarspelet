include settings.mk
export PATH := $(PATH):$(VBCC)/bin


TRG = target/mastarspelet


all: $(TRG).kick13 $(TRG).aos68k


$(TRG).kick13: src/mastarspelet.c target
	vc +$(VBCC)/config/kick13 -lamiga -o Librairies-no-inline-ok -D_NO_INLINE -o $@ $<


$(TRG).aos68k: src/mastarspelet.c target
	vc +$(VBCC)/config/aos68k -lamiga -o Librairies-no-inline-ok -D_NO_INLINE -o $@ $<


target:
	mkdir -p target
